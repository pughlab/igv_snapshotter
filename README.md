# README #

### What is this repository for? ###

* This script is useful for taking IGV screen shots of multiple regions across 
  multiple samples.
* V0.2

### Quick Snapshot Method

Make sure '/mnt/work1/users/pughlab/src/IGVSnapshot' is in your path, then enter 'snap' followed by (optionally) the bamfile(s) and coordinate(s) you wish to snapshot into the command line on samwise.

There are two options, automatically detected based on the number of arguments provided to the script:  


```
#!bash
snap coordinate1,coordinate2…
```
This will snapshot all bam files in your current directory at the coordinate(s) provided  

OR
```
#!bash
 snap file_1.bam,file_2.bam… coordinate1,coordinate2… 
```
This will snapshot the bam files provided at the coordinate(s) provided. If only two bam files are provided, it will also combined each pair of images at the same locus (useful for tumour/normal comparison)

### Extended Method   

This method, which includes the manual construction of a configuration file is better for snapshots of many loci from a maf file, or if you wish to customize the IGV settings etc.  

**Required:**  

1. Python  

Load python using “module load python” at the command line or include this in a small sh script to run the tool.  

2. Python Script:  

Script can be found here:  

/mnt/work1/users/pughlab/src/IGVSnapshot/IGV_Snap.py  

3. Config file:  

Template can be found here:  

/mnt/work1/users/pughlab/src/IGVSnapshot/Template_IGV_Snap.cfg  
Explained Below:  

[Dir]  
inputdir = /path/to/bam/files/  
outputdir = /path/to/output/directory/  
bamfiles = All  

**inputdir:** Where your bam files are. All the files you wish to load must be in the same directory. Creating a new directory with symlinks is a useful method to combine bams from various locations.  

**outputdir:** Where you wish to output the results from the tool. Two folders “images” and “batch_scripts” will be created here when the script begins.  

**bamfiles:** Either “All”, in which case all the bam files in your “inputdir” will be loaded and snapshotted sequentially. Or a list of bam files that you wish to be analyzed, of the form: File_1.bam,File_2.bam,File_3.bam,…  

[Loci]  
filetype = oncotator  
coordinates = path/to/coordinate/file/  
labels = label_1,label_2,label_3,etc  

**filetype:** The format of the coordinate file to be supplied. Options are: 'oncotator', 'mutect' or 'loci' (see below)   
**coordinates:** The full path to the coordinates file to be used. Instead of an oncotator or mutect file, a simple list of coordinates can also be used, with “filetype = loci'. This should be of the form: chr:57478807,chr20:57484420,chr20:57478707-57484520  
**labels:** If a list of coordinates is given, you may want a human readable label (such as gene names) added to the filenames of output images. These can be provided in the same order as their corresponding loci.  

[IGV]  
IGVscript = /mnt/work1/users/pughlab/bin/IGV_2.3.2/igv_snapshop.sh    
genome = genome hg19  
panelheight = maxPanelHeight 1500  
ntrange = 40  
collapse = False  

**IGVscript:** This sh script has been modified to facilitate use with this tool. **genome:** This is a direct command to IGV, can be changed if your alignment was performed using a different reference.  
**panelheight:** This adjusts how “tall” your images can be based on the depth. If you would like to include all the reads at your site of interest, this can be set very high (ex. 'maxPanelHeight 10000'), yielding a tall and thin image.  
**ntrange:** The window you wish to be visible in the output images around single nucleotide loci (if the locus of interest is provided in the form 'chr:start-end', that is the window that will be used.  
**collapse:** Whether you would like to squish reads to make more visible in a smaller area.  

[Display]  
servernum = :1  
screennum = 0  

**servernum:** Can be changed to any integer to avoid running into other users/instances running the tool.  
**screennum:** Should be left as 0.  

[TN_Pairs]  
tnMatch = Tumour1.bam,Normal1.bam,Tumour2.bam,Normal2.bam   
**tnMatch:** Options are 'True', 'False', or a list of tumour normal pairs. If True, bam file names must be of the form: SamplName1_T.bam SampleName1_N.bam SamplName2_T.bam SampleName2_N.bam, with _T and _N denoting tumour and normal, respectively. If False, no images will be combined.   
Alternatively, a list of tumour/normal pairs can be provided in the form:   Tumour1.bam,Normal1.bam,Tumour2.bam,Normal2.bam,…  


Sample commands to run the tool:  

**with the config file in your current directory:**  
module load python  
python /mnt/work1/users/pughlab/src/IGVSnapshot/IGV_Snap.py IGV_Snap.cfg