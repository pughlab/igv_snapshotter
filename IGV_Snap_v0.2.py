#This wrapper takes bam files and genomic loci and saves IGV screen shots of the region for each locus

################################################################################################################
# USAGE:
#  module load python
#  python /mnt/work1/users/pughlab/src/IGVSnapshot/IGV_Snap_v0.2.py IGV_Snap.cfg
#
################################################################################################################

#!/usr/bin/env python

#usage: python IGV_Snap.py inputdir outputdir coordinate/file
import os
import sys
import subprocess
import re
import ConfigParser
import shlex

###############
###FUNCTIONS###
###############

#Create directories to save files
def dircreate(outputdir,batchdir,imagedir):
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    if not os.path.exists(batchdir):
        os.makedirs(batchdir)
    if not os.path.exists(imagedir):
        os.makedirs(imagedir)

## checks if the input dir is not empty
def checkinput(inputdir):
    filelist = []
    for root, dirs, files in os.walk(inputdir, topdown=False):
        for item in files:
            if item:
                filelist.append(item)
            else:
                print "error: input directory is empty"
            
    return filelist

#Parse list of loci to be recognizeable by igv
def ParseLoci(coordinates):
    if os.path.exists(coordinates):
        loci = []
        genenames = []
        fil = open(coordinates, 'r')
        first = fil.readline()
        match = re.search('#', first)
        
        #loop through commented lines
        while str(match) != 'None':
            first = fil.readline()
            match = re.search('#', first)
            
        if filetype == "mutect":    
            #read in consecutive lines    
            for line in fil.readlines():
                splitline = line.split('\t')
                chromosome = splitline[0]
                locus = splitline[1]
                
                if ntrange != '40':
                    locusstart = int(locus) - (ntrange)/2
                    locusend = int(locus) + (ntrange)/2
                    locus = '-'.join([str(locusstart),str(locusend)])
                        
                locusstr = ''.join([chromosome,":",locus])
                loci.append(locusstr)
                genenames.append("")
            
        elif filetype.lower() == "oncotator":
            #read in consecutive lines    
            for line in fil.readlines():
                splitline = line.split('\t')
                chromosome = splitline[4]
                locus = splitline[5]
                
                if ntrange != '40':
                    locusstart = int(locus) - (ntrange)/2
                    locusend = int(locus) + (ntrange)/2
                    locus = '-'.join([str(locusstart),str(locusend)])
                        
                locusstr = ''.join(["chr",chromosome,":",locus])
                loci.append(locusstr)
                genenames.append(splitline[0])
        fil.close()
        
    else:
        loci = coordinates
        genenames = [''] * len(loci)
    lociAndnames = [loci,genenames]
    return lociAndnames



#Create the batch script to run in IGV
def batchscript(bam, loci, genenames, collapse, commands):
    imagelist = []
    os.chdir(batchdir)
    batchfile = '.'.join([os.path.basename(bam), "batch"])
    job = open(batchfile, 'w')
    loadbam = ''.join(["load ", inputdir,"/",bam])
    snapshotdir = ' '.join(["snapshotDirectory", imagedir])
    if collapse.lower() == 'true':
        lines = "new", nl, genome, nl, loadbam, nl, panelheight, nl, "collapse", nl
    else:
        lines = "new", nl, genome, nl, loadbam, nl, panelheight, nl
        
    job.writelines(lines)
    
    if type(loci) != str:
        for i in range(0, len(loci)):
            gotolocus = ' '.join(["goto", loci[i]])
            sortbase = "sort base"
            imagefile = ''.join([imagedir,"/",os.path.basename(bam),"_",genenames[i],"_",loci[i],".png"])
            snapshot = ' '.join(["snapshot",imagefile])
            lines = gotolocus, nl, sortbase, nl, commands, nl, snapshot, nl
            job.writelines(lines)
            imagelist.append (imagefile)
    else:
        gotolocus = ' '.join(["goto", loci])
        sortbase = "sort base"
        imagefile = ''.join([imagedir,"/",os.path.basename(bam),"_",genenames,"_",loci,".png"])
        snapshot = ' '.join(["snapshot",imagefile])
        lines = gotolocus, nl, sortbase, nl, commands, nl, snapshot, nl
        job.writelines(lines)
        imagelist.append (imagefile)
            
    job.writelines("exit")
    
    batchreturn = [batchfile, imagelist]
    
    return batchreturn
                    

## Find Tumour/Normal pairs based on naming convention "SampleName_T*.bam/SampleName_N*.bam"
def findTNPairs(imagelists, tnMatch):
    
    normimages = []
    tumimages = []
    
    if type(tnMatch)!=list:
        
        for i in range(0, len(imagelists)):
            match = re.search('^(.+)_([Nn])', os.path.basename(imagelists[i][0]))
            if str(match) != 'None':
                normimages.extend(imagelists[i])
                for j in range(0, len(imagelists)):
                    tMatch = re.search('_'.join([match.group(1),"T"]), os.path.basename(imagelists[j][0]))
                    if str(tMatch) != 'None':
                        tumimages.extend(imagelists[j])
                
    else:
        for h in range(0, len(tnMatch), 2):
            for j in range(0, len(imagelists)):
                tMatch = re.search(tnMatch[h], os.path.basename(imagelists[j][0]))
                if str(tMatch) != 'None':
                    tumimages.extend(imagelists[j])
                else:
                    nMatch = re.search(tnMatch[h+1], os.path.basename(imagelists[j][0]))
                    if str(nMatch) != 'None':
                        normimages.extend(imagelists[j])
    
    pairlist = zip(normimages,tumimages)    
    return pairlist

## Concatenate tumour normal pairs
def tnAppend(tnpairs,tnMatch):
    if type(tnMatch)!=list:
        for filepair in tnpairs:
            matchbase = re.search('^(.+)_([TtNn])', os.path.basename(filepair[0]))
            basename = matchbase.group(1)
            match = re.search('chr(.+)', os.path.basename(filepair[0]))
            locus = match.group()
            matchgene = re.search('^(.+)_(.+)_chr(.+)', os.path.basename(filepair[0]))
            gene = matchgene.group(2)
            filename = "".join([imagedir,"/",basename,"_TN_Pair_",gene,"_",locus])
            print filename
            cmd = " ".join(["convert", "-background", "'#000000'", filepair[0], "-append", filepair[1], "-gravity north", "+append +repage", filename])
            print cmd
            os.system(cmd)
    else:
        for filepair in tnpairs:
            filename = "".join([imagedir,"/TN_Pair_",os.path.basename(filepair[0]),"_",os.path.basename(filepair[1])])
            print filename
            cmd = " ".join(["convert", "-background", "'#000000'", filepair[0], "-append", filepair[1], "-gravity north", "+append +repage", filename])
            print cmd
            os.system(cmd)

## Create screen(s)
def CreateScreen(servernum,screennum):
    #screen = ["Xvfb",servernum,"-screen",screennum,"3840x2160x24&"]
    screen = ["Xvfb",servernum,"-screen",screennum,"3840x2160x24&"]
    #proc = subprocess.Popen(screen)
    proc = subprocess.Popen(screen,shell=True)
    return proc.pid

## Run IGV batch commands
def runIGVBatch(batchfile):
    runIGV = ' '.join(["sh", IGVscript,"--batch", batchfile])
    os.system(runIGV)
    

############
### MAIN ###
############

## Variables
nl = "\n"
cwd = os.getcwd()

## Read the parameters from the config file ###

config = ConfigParser.RawConfigParser()
config.read(sys.argv[1])

#de-bug config.read('/mnt/work1/users/pughlab/projects/Vulvar_Carcinoma_Clarke/Exome/IGV_Snapshots/Indelgenotyper/IGV_Snap.cfg')

inputdir = config.get('Dir', 'inputdir')
outputdir = config.get('Dir', 'outputdir')
bamfiles = config.get('Dir', 'bamfiles')

coordinates = config.get('Loci', 'coordinates')
filetype = config.get('Loci', 'filetype')
labels = config.get('Loci', 'labels')

IGVscript = config.get('IGV', 'IGVscript')
genome = config.get('IGV', 'genome')
panelheight = config.get('IGV', 'panelheight')
ntrange = config.get('IGV', 'ntrange')
collapse = config.get('IGV', 'collapse')
commands = config.get('IGV', 'commands')

servernum = config.get('Display', 'servernum')
screennum = config.get('Display', 'screennum')

tnMatch = config.get('TN_Pairs', 'tnMatch')

batchdir = '/'.join([outputdir, "batch_scripts"])
imagedir =  '/'.join([outputdir, "images"])


## Create the output directories
dircreate(outputdir,batchdir,imagedir)

## Retrieve list of files in input directory OR provided list of bam files
if bamfiles.lower() == 'all':
    filelist = checkinput(inputdir)
    
    ## Keep only the bam files in the directory
    bamlist = []
    for item in filelist:
        if item.endswith('.bam'):
            bamlist.append(item)
    
    if bamlist == []:
        print "error: no BAM files in input directory"
        sys.exit()
    
else:
    bamlist = bamfiles.split(",")
    
## Get the coordinates and gene names of loci for snapshots
if filetype.lower() == 'loci':
    loci = coordinates.split(",")
    if labels.lower()!='none':
        genenames = labels.split(",")
    else:
        genenames = loci
else:
    lociAndnames = ParseLoci(coordinates)
    loci = lociAndnames[0]
    genenames = lociAndnames[1]

## Start a Xfvb screen and collect the job number so the screen can be closed when the script has finished running
pid = CreateScreen(servernum,screennum)

## Start IGV and take snapshots for each bam file
imagelists = []
ind=0

for bam in bamlist:
    batchreturn = batchscript(bam, loci, genenames, collapse, commands)
    batchfile = batchreturn[0]
    if imagelists == []:
        imagelists = batchreturn[1]
        ind=1
    elif ind == 1:
        imagelists = [imagelists,batchreturn[1]]
        ind=2
    else:
        imagelists.append(batchreturn[1])
    
    runIGVBatch(batchfile) 

## #Merge Tumour and Normal screen caps for each locus into one image
if not tnMatch.lower()=='false':
    if not tnMatch.lower()=='true':
        tnMatch = tnMatch.split(",")
    tnpairs = findTNPairs(imagelists,tnMatch)
    tnAppend(tnpairs,tnMatch)


## Close the virtual screen
os.system(' '.join(["kill",str(pid)]))

